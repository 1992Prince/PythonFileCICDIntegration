import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.remote.webdriver import WebDriver
import os

from selenium.webdriver.chrome.options import Options as ChromeOptions


def test_dologin():
    print("First testcase")

    options = ChromeOptions()
    options.browser_version = 'latest'
    options.platform_name = 'Windows 11'
    sauce_options = {}
    sauce_options['build'] = 'selenium-build-V1L6Y'
    sauce_options['name'] = '<LetCode Build_3>'
    options.set_capability('sauce:options', sauce_options)

    username = os.environ.get('UserName')
    key = os.environ.get('key')

    url = "https://"+username+":"+key+"@ondemand.eu-central-1.saucelabs.com:443/wd/hub"
    url1 = "https://oauth-princepandey155-40d9a:31f0ac37-f7c2-42b0-a635-ab59f386adc5@ondemand.eu-central-1.saucelabs.com:443/wd/hub"
    driver = webdriver.Remote(command_executor=url, options=options)

    driver.get('https://letcode.in/edit')
    driver.maximize_window()

    driver.implicitly_wait(30)

    driver.find_element(By.ID, "fullName").send_keys('Chunnu Madam')
    driver.find_element(By.ID, "clearMe").clear()
    value = driver.find_element(By.ID, "dontwrite").get_property("value")
    print(f"Text box value is : {value}")

    title = driver.title

    print(f"Title of page is : {title}")

    assert title == "Interact with Input fields"

    driver.quit()

